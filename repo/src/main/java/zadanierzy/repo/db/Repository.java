package zadanierzy.repo.db;

public interface Repository<TEntity> {
	
	TEntity withId(int id);
	void add (TEntity entity);
	void modify (TEntity entity);
	void remove (TEntity entity);
}
