package zadanierzy.repo.db;

public interface RepositoryCatalog {
	
	ActorRepository actor();
	DirectorRepository director();
	TvSeriesRepository tvseries();
	SeasonRepository season();
	EpisodeRepository episode();
}
