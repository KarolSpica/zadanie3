package zadanierzy.repo.db;

import zadanietrzy.repo.domain.Director;

public interface DirectorRepository extends Repository<Director> {

}
