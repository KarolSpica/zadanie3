package zadanietrzy.repo.db.catalogs;

import java.sql.Connection;

import zadanierzy.repo.db.ActorRepository;
import zadanierzy.repo.db.DirectorRepository;
import zadanierzy.repo.db.EpisodeRepository;
import zadanierzy.repo.db.RepositoryCatalog;
import zadanierzy.repo.db.SeasonRepository;
import zadanierzy.repo.db.TvSeriesRepository;
import zadanietrzy.repo.db.repos.HsqlActorRepository;
import zadanietrzy.repo.db.repos.HsqlDirectorRepository;
import zadanietrzy.repo.db.repos.HsqlEpisodeRepository;
import zadanietrzy.repo.db.repos.HsqlSeasonRepository;
import zadanietrzy.repo.db.repos.HsqlTvSeriesRepository;

public class HsqlRepositoryCatalog implements RepositoryCatalog{
	
	Connection connection;
	
	public HsqlRepositoryCatalog(Connection connection) {
		this.connection = connection;
	}

	public ActorRepository actor() {
		return new HsqlActorRepository(connection);
	}
	public DirectorRepository director() {
		return new HsqlDirectorRepository(connection);
	}
	public TvSeriesRepository tvseries() {
		return new HsqlTvSeriesRepository(connection);
	}

	public SeasonRepository season() {
		return new HsqlSeasonRepository(connection);
	}

	public EpisodeRepository episode() {
		return new HsqlEpisodeRepository(connection);
	}
	

}
