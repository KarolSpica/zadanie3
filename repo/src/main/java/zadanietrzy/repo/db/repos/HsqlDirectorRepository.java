package zadanietrzy.repo.db.repos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import zadanierzy.repo.db.DirectorRepository;
import zadanietrzy.repo.db.mapper.DirectorMapper;
import zadanietrzy.repo.domain.Director;

public class HsqlDirectorRepository implements DirectorRepository{
	
	private String tableName = "Director";
	private Connection connection;
	private DirectorMapper directorMapper;
	private String createDirectorTable = ""
			+ "CREATE TABLE Director("
			+ "id bigint GENERATED BY DEFAULT AS IDENTITY,"
			+ "name VARCHAR(20),"
			+ "birth DATE,"
			+ "biography VARCHAR(80)"
			+ ")";
	
	public HsqlDirectorRepository(Connection connection) {
		this.connection = connection;
		this.directorMapper = new DirectorMapper(connection);
		
		ChceckTable checkTable = new ChceckTable();
		checkTable.checkIfTableExsist(connection, tableName, createDirectorTable);
	}

	public Director withId(int id) {
		return directorMapper.find((long)id);
	}

	public void add(Director director) {
		directorMapper.add(director);
	}

	public void modify(Director director) {
		directorMapper.update(director);
		
	}

	public void remove(Director director) {
		directorMapper.remove((long)director.getId());
	}
	
	
}
