package zadanietrzy.repo.db.mapper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import zadanierzy.repo.db.RepositoryCatalog;
import zadanietrzy.repo.db.catalogs.HsqlRepositoryCatalog;
import zadanietrzy.repo.domain.Season;
import zadanietrzy.repo.domain.TvSeries;

public class TvSeriesMapper extends AbstractMapper<TvSeries>{

	private String url = "jdbc:hsqldb:hsql://localhost/testdb";
	private static final String COLUMNS = "id, name";
	public static final String FIND_STMT = "SELECT " + COLUMNS + " FROM tvseries WHERE id=?";
	public static final String INSERT_STMT = "INSERT INTO tvseries(name) VALUES(?)";
	public static final String UPDATE_STMT = "UPDATE tvseries SET (name)=(?) WHERE id=?";
	public static final String DELETE_STMT = "DELETE FROM tvseries WHERE id=?";
	
	public TvSeriesMapper(Connection connection) {
		super(connection);
	}

	@Override
	protected String findStatement() {
		return FIND_STMT;
	}

	@Override
	protected String insertStatement() {
		return INSERT_STMT;
	}

	@Override
	protected String updateStatement() {
		return UPDATE_STMT;
	}

	@Override
	protected String removeStatement() {
		return DELETE_STMT;
	}

	@Override
	protected TvSeries doLoad(ResultSet rs) throws SQLException {
		TvSeries tvSeries = new TvSeries();
		tvSeries.setName(rs.getString("name"));
		tvSeries.setId(rs.getInt("id"));
		return tvSeries;
	}

	@Override
	protected void parametrizeInsertStatement(PreparedStatement statement, TvSeries tvSeries) throws SQLException {
		statement.setString(1, tvSeries.getName());
		
	}

	@Override
	protected void parametrizeUpdateStatement(PreparedStatement statement, TvSeries tvSeries) throws SQLException {
		parametrizeInsertStatement(statement, tvSeries);
		statement.setLong(2, tvSeries.getId());
		
	}

	@Override
	public void add(TvSeries tvSeries) {
		try {
		connection = DriverManager.getConnection(url);
    	RepositoryCatalog catalog = new HsqlRepositoryCatalog(connection);
		if(tvSeries.getSeasons()!=null) {
			for(Season season : tvSeries.getSeasons()) {
				catalog.season().add(season);
			}
		}
	
	} catch(SQLException e) {
		e.printStackTrace();
	}
		
		super.add(tvSeries);
	
	
}
}
