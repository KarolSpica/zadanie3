package zadanietrzy.repo.db.mapper;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import zadanietrzy.repo.domain.Episode;

public class EpisodeMapper extends AbstractMapper<Episode> {

	private static final String COLUMNS = "id, name, episodeNumber, duration, dateOfRelease, seasonId";
	public static final String FIND_STMT = "SELECT " + COLUMNS + " FROM episode WHERE id=?";
	public static final String INSERT_STMT = "INSERT INTO episode(name, episodeNumber, duration, dateOfRelease,seasonId) VALUES(?,?,?,?,?)";
	public static final String UPDATE_STMT = "UPDATE episode SET (name, episodeNumber, duration, dateOfRelease)=(?,?,?,?) WHERE id=?";
	public static final String DELETE_STMT = "DELETE FROM epiosde WHERE id=?";
	
	public EpisodeMapper(Connection connection) {
		super(connection);
	}

	@Override
	protected String findStatement() {
		return FIND_STMT;
	}

	@Override
	protected String insertStatement() {
		return INSERT_STMT;
	}

	@Override
	protected String updateStatement() {
		return UPDATE_STMT;
	}

	@Override
	protected String removeStatement() {
		return DELETE_STMT;
	}

	@Override
	protected Episode doLoad(ResultSet rs) throws SQLException {
		Episode episode = new Episode();
		episode.setName(rs.getString("name"));
		episode.setEpisodeNumber(rs.getInt("episodeNumber"));
		episode.setDuration(rs.getInt("duration"));
		episode.setReleaseDate(rs.getDate("dateOfRelease").toLocalDate());
		episode.setId(rs.getInt("id"));
		return episode;
	}

	@Override
	protected void parametrizeInsertStatement(PreparedStatement statement, Episode episode) throws SQLException {
		int foreignKey = 0;
		ResultSet resultSet;
		PreparedStatement checkSeasonId;
		try {
			checkSeasonId = connection.prepareStatement("SELECT id FROM episode WHERE name=(?)");
			checkSeasonId.setString(1, episode.getSeasonName());
			resultSet =checkSeasonId.executeQuery();
			while(resultSet.next()) {
				foreignKey=resultSet.getInt(1);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		statement.setString(1,episode.getName());
		statement.setInt(2, episode.getEpisodeNumber());
		statement.setInt(3, episode.getDuration());
		statement.setDate(4,  Date.valueOf(episode.getReleaseDate()));
		statement.setInt(5, foreignKey);
		
	}

	@Override
	protected void parametrizeUpdateStatement(PreparedStatement statement, Episode episode) throws SQLException {
		statement.setString(1,episode.getName());
		statement.setInt(2, episode.getEpisodeNumber());
		statement.setInt(3, episode.getDuration());
		statement.setDate(4,  Date.valueOf(episode.getReleaseDate()));
		statement.setInt(5, episode.getId());
		
	}
	
}
