package zadanietrzy.repo.db.mapper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import zadanierzy.repo.db.RepositoryCatalog;
import zadanietrzy.repo.db.catalogs.HsqlRepositoryCatalog;
import zadanietrzy.repo.domain.Episode;
import zadanietrzy.repo.domain.Season;

public class SeasonMapper extends AbstractMapper<Season>{
	
	private String url = "jdbc:hsqldb:hsql://localhost/testdb";
	private static final String COLUMNS = "id, seasonNumber, yearOfRelease";
	public static final String FIND_STMT = "SELECT " + COLUMNS + " FROM seasons WHERE id=?";
	public static final String INSERT_STMT = "INSERT INTO seasons(seasonNumber, yearOfRelease,serialid) VALUES(?,?,?)";
	public static final String UPDATE_STMT = "UPDATE seasons SET (seasonNumber, yearOfRelease)=(?,?) WHERE id=?";
	public static final String DELETE_STMT = "DELETE FROM seasons WHERE id=?";


	public SeasonMapper(Connection connection) {
		super(connection);
	}

	@Override
	protected String findStatement() {
		return FIND_STMT;
	}

	@Override
	protected String insertStatement() {
		return INSERT_STMT;
	}

	@Override
	protected String updateStatement() {
		return UPDATE_STMT;
	}

	@Override
	protected String removeStatement() {
		return DELETE_STMT;
	}

	@Override
	protected Season doLoad(ResultSet rs) throws SQLException {
		Season season = new Season();
		season.setSeasonNumber(rs.getInt("seasonNumber"));
		season.setYearOfRelease(rs.getInt("yearOfRelease"));
		season.setId(rs.getInt("id"));
		return season;
	}

	@Override
	protected void parametrizeInsertStatement(PreparedStatement statement, Season season) throws SQLException {
		int foreignKey=0;
		ResultSet resultSet;
		PreparedStatement checkTvSeriesId;
		try {
			checkTvSeriesId = connection.prepareStatement("SELECT id FROM TVSERIES WHERE name=(?)");
			checkTvSeriesId.setString(1, season.getSezonName());
			resultSet = checkTvSeriesId.executeQuery();
			while(resultSet.next()) {
				foreignKey=resultSet.getInt(1);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		statement.setInt(1, season.getSeasonNumber());
		statement.setInt(2, season.getYearOfRelease());
		statement.setInt(3, foreignKey);
		
	}

	@Override
	protected void parametrizeUpdateStatement(PreparedStatement statement, Season season) throws SQLException {
		statement.setInt(1, season.getSeasonNumber());
		statement.setInt(2, season.getYearOfRelease());
		statement.setInt(3, season.getId());
		
	}

	@Override
	public void add(Season season) {
		try {
			connection = DriverManager.getConnection(url);
			RepositoryCatalog catalog = new HsqlRepositoryCatalog(connection);
			if(season.getEpizodes()!=null) {
				for(Episode episode : season.getEpizodes()) {
					catalog.episode().add(episode);
				}
			}
		} catch(SQLException e) {
			e.printStackTrace();;
		}
		super.add(season);
	}
	
	
	
}
