package zadanietrzy.repo.domain;

import java.util.List;

public class TvSeries implements DomainObject{
	
	private int id;
	private String name;
	List<Season> seasons;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Season> getSeasons() {
		return seasons;
	}
	public void setSeasons(List<Season> seasons) {
		this.seasons = seasons;
	}
	
	
	
	
}
